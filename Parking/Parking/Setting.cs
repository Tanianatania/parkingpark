﻿namespace Parking
{
    public static class Setting
    {
        private static readonly decimal balance = 0;
        private static readonly int countOfPlace = 10;
        private static readonly int pediodOfTime = 5;
        private static readonly decimal carRate = 2;
        private static readonly decimal trakRate = 5;
        private static readonly decimal busRate = 3.5M;
        private static readonly decimal motorcycleRate = 1;
        private static readonly decimal fine = 2.5M;
        public static string FilePath = @"..\..\..\Transactions.log";

        public static int GetPeriodOfTime()
        {
            return pediodOfTime;
        }

        public static decimal GetFine()
        {
            return fine;
        }

        public static decimal GetBalance()
        {
            return balance;
        }

        public static decimal GetRate(string name)
        {
            switch (name)
            {
                case "car":
                    return carRate;
                case "truck":
                    return trakRate;
                case "bus":
                    return busRate;
                case "motorbike":
                    return motorcycleRate;
                default:
                    return 0;
            }
        }

        public static int GetCountOfPlace()
        {
            return countOfPlace;
        }
    }
}