﻿namespace Parking.Cars
{
    public class Motorbike : Car
    {
        public Motorbike()
        {
            Name = Const.Motorbike;
        }

        public Motorbike(string carNumber, decimal balance) : base(carNumber, balance)
        {
            Name = Const.Motorbike;
        }
    }
}