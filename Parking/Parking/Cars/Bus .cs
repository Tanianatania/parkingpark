﻿namespace Parking.Cars
{
    internal class Bus : Car
    {
        public Bus()
        {
            Name = Const.Bus;
        }


        public Bus(string carNumber, decimal balance) : base(carNumber, balance)
        {
            Name = Const.Bus;
        }
    }
}