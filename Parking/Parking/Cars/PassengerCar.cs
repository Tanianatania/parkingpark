﻿namespace Parking.Cars
{
    public class PassengerCar : Car
    {
        public PassengerCar()
        {
            Name = Const.PassengerCar;
        }

        public PassengerCar(string carNumber, decimal balance) : base(carNumber, balance)
        {
            Name = Const.PassengerCar;
        }
    }
}