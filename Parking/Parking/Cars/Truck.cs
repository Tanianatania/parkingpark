﻿namespace Parking.Cars
{
    public class Truck : Car
    {
        public Truck()
        {
            Name = Const.Truck;
        }

        public Truck(string carNumber, decimal balance) : base(carNumber, balance)
        {
            Name = Const.Truck;
        }
    }
}