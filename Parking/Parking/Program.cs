﻿using System;
using Parking.Cars;
using Parking.Interfaces;

namespace Parking
{
    internal class Program
    {
        public static bool Exit()
        {
            bool result;
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine(" Do you want continue?\n" +
                              "\t1 - Yes\n" +
                              "\t2 - No");
            Console.ResetColor();
            var value = Console.ReadLine();
            var key = int.Parse(value);
            if (key == 1) return false;

            if (key == 2) return true;

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("The number should be in the range of 1 to 2");
            Console.ResetColor();
            result = Exit();
            return result;
        }

        public static string GetCarNumber()
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("Enter car number:");
            Console.ResetColor();
            var carNumber = Console.ReadLine();
            return carNumber;
        }

        public static decimal GetCarStartBalance()
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("Enter car start balance:");
            Console.ResetColor();
            var carBalance = 0M;
            try
            {
                carBalance = Convert.ToDecimal(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("The input data should be number");
            }
            catch (OverflowException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Value is too large or too small");
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.Message);
            }
            return carBalance;
        }

        public static ICar CreateNewCar()
        {
            var exit = false;
            ICar newCar = null;

            while (exit != true)
            {
                bool isDo = false;
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Select type of your car:\n" +
                                  "\t1 - {0}\n" +
                                  "\t2 - {1}\n" +
                                  "\t3 - {2}\n" +
                                  "\t4 - {3}", Const.PassengerCar, Const.Bus, Const.Motorbike, Const.Truck);
                try
                {
                    var value = Console.ReadLine();
                    var type = int.Parse(value);
                    isDo = true;
                    string carName;
                    decimal carBalance;
                    switch (type)
                    {
                        case 1:
                            carName = GetCarNumber();
                            if (carName!="")
                            {
                                carBalance = GetCarStartBalance();
                                if (carBalance!=0)
                                {
                                    newCar = new PassengerCar
                                    {
                                        CarNumber = carName,
                                        Balance = carBalance
                                    };
                                }                                  
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Car number can`t be empty");
                                Console.ResetColor();
                            }
                            
                            exit = true;
                            break;
                        case 2:
                            carName = GetCarNumber();
                            if (carName != "")
                            {
                                carBalance = GetCarStartBalance();
                                if (carBalance != 0)
                                {
                                    newCar = new Bus
                                    {
                                        CarNumber = carName,
                                        Balance = carBalance
                                    };
                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Car number can`t be empty");
                                Console.ResetColor();
                            }
                            exit = true;
                            break;
                        case 3:
                            carName = GetCarNumber();
                            if (carName != "")
                            {
                                carBalance = GetCarStartBalance();
                                if (carBalance != 0)
                                {
                                    newCar = new Motorbike
                                    {
                                        CarNumber = carName,
                                        Balance = carBalance
                                    };
                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Car number can`t be empty");
                                Console.ResetColor();
                            }
                            exit = true;
                            break;
                        case 4:
                            carName = GetCarNumber();
                            if (carName != "")
                            {
                                carBalance = GetCarStartBalance();
                                if (carBalance != 0)
                                {
                                    newCar = new Truck
                                    {
                                        CarNumber = carName,
                                        Balance = carBalance
                                    };
                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Car number can`t be empty");
                                Console.ResetColor();
                            }
                            exit = true;
                            break;
                        default:
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("The number should be in the range of 1 to 4");
                            Console.ResetColor();
                            exit = Exit();
                            break;
                    }
                }
                catch (FormatException)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("The input data should be number");
                }
                catch (OverflowException)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Value is too large or too small");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    if (isDo == false)
                    {
                        exit = Exit();
                    }
                }
            }
            return newCar;
        }

        private static void Main()
        {
            var park = Parking.GetInstance();

            park.Run();
            var exit = false;

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(" Hello, welcome to out Parking Park))");
            Console.ResetColor();
            while (exit != true)
            {
                bool isDo = false;
                Console.ResetColor();
                Console.WriteLine(" Please choose from the list what you would like to do:");
                Console.WriteLine("\t1 - find out the current balance of Parking Park\n" +
                                  "\t2 - Find out the amount of money earned in the last minute\n" +
                                  "\t3 - Find out the count of free place in Parking Park\n" +
                                  "\t4 - Show all Parking Park transactions in the last minute\n" +
                                  "\t5 - Print the entire transaction history\n" +
                                  "\t6 - Display the list of all Vehicles\n" +
                                  "\t7 - Put a Vehicle on Parking\n" +
                                  "\t8 - Take away the Parking Truck\n" +
                                  "\t9 - Refill the balance of the Vehicle\n" +
                                  "\t10 - Exit");
                try
                {
                    var value = Console.ReadLine();
                    var key = int.Parse(value);
                    isDo = true;
                    Console.ForegroundColor = ConsoleColor.Green;
                    string carNumber;
                    switch (key)
                    {
                        case 1:
                            Console.WriteLine("Current balance of Parking Park: {0}", park.GetTotalBalance());
                            exit = Exit();
                            break;
                        case 2:
                            Console.WriteLine("Balance per minute: {0}", park.GetBalancePerMinute());
                            exit = Exit();
                            break;
                        case 3:
                            Console.WriteLine("Count of free place: {0}", park.CountOfAvailablePlace());
                            exit = Exit();
                            break;
                        case 4:
                            park.DisplayTransactions();
                            exit = Exit();
                            break;
                        case 5:
                            park.DisplayAllTransactions();
                            exit = Exit();
                            break;
                        case 6:
                            park.DisplayCarList();
                            exit = Exit();
                            break;
                        case 7:
                            ICar car = CreateNewCar();
                            if (car != null)
                            {
                                park.AddCar(car);
                                exit = Exit();
                            }
                            break;
                        case 8:
                            carNumber = GetCarNumber();
                            if (park.FindCarByCarNumber(carNumber) != null)
                            {
                                park.RemoveCar(carNumber);
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Car with with number is not exist");
                                Console.ResetColor();
                            }

                            exit = Exit();
                            break;
                        case 9:
                            carNumber = GetCarNumber();
                            if (carNumber == "")
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Car can not have null car number");
                                Console.ResetColor();
                                exit = Exit();
                                break;
                            }

                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            Console.WriteLine("Enter count of money:");
                            Console.ResetColor();
                            if (park.FindCarByCarNumber(carNumber) != null)
                            {
                                var money = Convert.ToDecimal(Console.ReadLine());
                                park.AddMoneyToCar(carNumber, money);
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Car with with number is not exist");
                                Console.ResetColor();
                            }

                            exit = Exit();
                            break;
                        case 10:
                            exit = true;
                            break;
                        default:
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("The number should be in the range of 1 to 10");
                            Console.ResetColor();
                            exit = Exit();
                            break;
                    }
                }
                catch (FormatException)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("The input data should be number");
                }
                catch (OverflowException)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Value is too large or too small");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    if (isDo == false)
                    {
                        exit = Exit();
                    }
                }
            }

            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("GoodBye!!)))");
            Console.ResetColor();
        }
    }
}