﻿using System;
using System.IO;

namespace Parking
{
    public class ParkingFile : IDisposable
    {
        private FileStream MyFileStream { get; }
        private StreamWriter MyStreamWriter { get; }

        public ParkingFile(string filePath)
        {
            MyFileStream = new FileStream(filePath, FileMode.Append);
            MyStreamWriter = new StreamWriter(MyFileStream);
        }

        public void AddTextToFile(string text)
        {
            MyStreamWriter?.WriteLine(text);
        }

        public void Dispose()
        {
            MyStreamWriter?.Dispose();
            MyFileStream?.Dispose();
        }
    }
}