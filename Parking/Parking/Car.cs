﻿using Parking.Interfaces;

namespace Parking
{
    public abstract class Car : ICar
    {
        public string Name { get; set; }
        public string CarNumber { get; set; }
        public decimal Balance { get; set; }

        public Car()
        {
            CarNumber = "";
            Balance = 0;
        }

        public Car(string carNumber, decimal balance)
        {
            CarNumber = carNumber;
            Balance = balance;
        }

        public decimal GetRate()
        {
            return Setting.GetRate(Name);
        }

        public void AddMoney(decimal money)
        {
            Balance += money;
        }

        public decimal RemoveMoney(decimal fine)
        {
            var salary = Setting.GetRate(Name);
            if (Balance < salary) salary *= fine;
            Balance -= salary;
            return salary;
        }
    }
}