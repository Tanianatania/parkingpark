﻿namespace Parking.Interfaces
{
    public interface ICar
    {
        string Name { get; set; }
        string CarNumber { get; set; }
        decimal Balance { get; set; }
        decimal GetRate();
        void AddMoney(decimal money);
        decimal RemoveMoney(decimal fine);
    }
}
