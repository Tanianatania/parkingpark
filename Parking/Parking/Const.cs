﻿namespace Parking
{
    public class Const
    {
        public static string Bus = "bus";
        public static string Truck = "truck";
        public static string PassengerCar = "car";
        public static string Motorbike = "motorbike";
    }
}
