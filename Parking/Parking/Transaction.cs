﻿using System;

namespace Parking
{
    public class Transaction
    {
        public DateTime Time { get; set; }
        public string CarType { get; set; }
        public string CarNumber { get; set; }
        public decimal Salary { get; set; }

        public Transaction(string carType, string carNumber, decimal salary, DateTime time)
        {
            CarType = carType;
            CarNumber = carNumber;
            Salary = salary;
            Time = time;
        }

        public void Dasplay()
        {
            Print.PrintRow(CarType, CarNumber, Salary.ToString(), Time.ToString());
        }
    }
}