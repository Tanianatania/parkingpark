﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Timers;
using Parking.Interfaces;

namespace Parking
{
    public class Parking
    {
        private decimal _balance;
        private int _countOfPlace;
        public static List<Transaction> Transactions = new List<Transaction>();
        public static List<ICar> Cars = new List<ICar>();
        private decimal _fine;

        private static readonly Lazy<Parking> Lazy =
            new Lazy<Parking>(() => new Parking
            {
                _balance = Setting.GetBalance(),
                _fine = Setting.GetFine(),
                _countOfPlace = Setting.GetCountOfPlace()
            });

        public static Parking GetInstance()
        {
            return Lazy.Value;
        }

        public void Run()
        {
            var period = Setting.GetPeriodOfTime();
            var timer = new Timer(period * 1000);
            timer.AutoReset = true;
            timer.Elapsed += AddTransaction;
            timer.Start();

            var fileTimer = new Timer(60000);
            fileTimer.AutoReset = true;
            fileTimer.Elapsed += WriteToFile;
            fileTimer.Start();
        }

        public void AddMoneyToCar(string carNumber, decimal money)
        {
            var car = FindCarByCarNumber(carNumber);
            car.AddMoney(money);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Balance was update");
            Console.ResetColor();
        }

        public void WriteToFile(object sender, ElapsedEventArgs e)
        {
            using (var file = new ParkingFile(Setting.FilePath))
            {
                foreach (var item in Transactions)
                {
                    var elem = item.CarType + " " + item.CarNumber + " " + item.Salary + " " + item.Time;
                    file.AddTextToFile(elem);
                }

                Transactions = new List<Transaction>();
            }
        }

        public void DisplayAllTransactions()
        {
            try
            {
                using (var sr = new StreamReader(Setting.FilePath))
                {
                    string line;
                    WriteTableTitle("Type", "Car Number", "Withdraw money", "Date");

                    while ((line = sr.ReadLine()) != null)
                    {
                        var result = line.Split(" ");
                        var time = result[3] + " " + result[4];
                        Print.PrintRow(result[0], result[1], result[2], time);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
        }

        public void AddTransaction(object sender, ElapsedEventArgs e)
        {
            foreach (var car in Cars)
            {
                var salary = car.RemoveMoney(_fine);

                var transaction = new Transaction(car.Name, car.CarNumber, salary, DateTime.Now);
                Transactions.Add(transaction);
                _balance += salary;
            }
        }

        private static void WriteTableTitle(params string[] titles)
        {
            Print.PrintRow(titles);
            for (var i = 0; i < Print.TableWidth; i++) Console.Write("-");
            Console.WriteLine();
        }

        public ICar FindCarByCarNumber(string carNumber)
        {
            ICar car = null;
            foreach (var item in Cars)
                if (item.CarNumber == carNumber)
                    car = item;
            return car;
        }

        public static bool IsCarNumberAvailable(string name)
        {
            bool res = true;
            foreach (var item in Cars)
                if (item.CarNumber == name)
                    res = false;
            return res;
        }

        public void AddCar(ICar car)
        {
            if (Cars.Count < _countOfPlace)
            {
                if (IsCarNumberAvailable(car.CarNumber))
                {
                    Cars.Add(car);
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Car was added");
                    Console.ResetColor();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("This car number is exist");
                    Console.ResetColor();
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("We don`t have free please. Sorry(((((((");
                Console.ResetColor();
            }
        }

        public void RemoveCar(string carNumber)
        {
            foreach (var car in Cars)
                if (car.CarNumber == carNumber)
                {
                    Cars.Remove(car);
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Car was remove");
                    Console.ResetColor();
                    return;
                }
        }

        public void DisplayCarList()
        {
            WriteTableTitle("Type", "Number", "Balance");
            foreach (var item in Cars) Print.PrintRow(item.Name, item.CarNumber, item.Balance.ToString(CultureInfo.InvariantCulture));
        }

        public void DisplayTransactions()
        {
            WriteTableTitle("Type", "Car Number", "Withdraw money", "Time");
            foreach (var item in Transactions) item.Dasplay();
        }

        public decimal GetTotalBalance()
        {
            return _balance;
        }

        public decimal GetBalancePerMinute()
        {
            decimal balance = 0;
            foreach (var item in Transactions) balance += item.Salary;
            return balance;
        }

        public int CountOfAvailablePlace()
        {
            return _countOfPlace - Cars.Count;
        }
    }
}